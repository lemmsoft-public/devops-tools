import * as express from 'express'
import {exec} from 'child_process'
import * as http from 'http'

/**
 * Contains the logic for starting performing automated deployments and monitoring their statuses.
 */
export class DevopsServer {
	/**
	 * @param config (required) - An object, containing the port, which the server must be started on.
	 */
	constructor(
		public config: {port: number}
	) {
	}

	/**
	 * Mounts the server routes.
	 * @returns Promise<boolean>
	 */
	listen(): Promise<void> {
		const instance = this;
		return (async function() {
			let app = express()
			app.get(
				'/deploy',
				instance.deploy()
			)
			app.get(
				'/monitorDeployment',
				instance.monitorDeployment()
			)
			await new Promise((resolve) => {
				http.createServer(app).listen(instance.config.port, () => {
					console.log(`[@lemmsoft/devops-tools/server]: server started on port ${instance.config.port}`)
					resolve()
				})
			})
		})()
	}

	/**
	 * 
	 * @returns express.RequestHandler
	 */
	deploy(): express.RequestHandler {
		return async function(req: express.Request, res: express.Response) {
			try {
				// const command = `touch ${decodeURIComponent(req.params.pathToFile)}`
				// console.log(`[@lemmsoft/devops-tools/server]: ${command}`)
				// await new Promise((resolve, reject) => {
				// 	exec(command, ((err, _stdout, stderr) => {
				// 		const error = err || stderr || null
				// 		if (error) {
				// 			reject(error)
				// 		}
				// 		console.log(`[@lemmsoft/devops-tools/server]: touch closed`)
				// 		resolve(true)
				// 	}))
				// })
				// res.status(200).end()

				// TODO: continuously update the status of the process and spin the below steps off into a separate process
				// TODO: validate the Authorization token & take the id from it
				// TODO: find the data in the database using the token id
				// TODO: check the repo whether there's a merge request
				// TODO: clone the repo
				// TODO: create folders and copy assets, according to the provided config
				// TODO: create a new docker container, according to the provided dockerfile
				// TODO: build the FE
				// TODO: create a new database
				// TODO: populate the new database with the data from the old one
				// TODO: check the static data files for insertion
				// TODO: insert static data, if needed
				// TODO: replace the old container with the new one
			}
			catch (e) {
				console.log('[@lemmsoft/devops-tools/server]: error:', e)
				res.status(500).end()
			}
		}
	}

	/**
	 * 
	 * @returns express.RequestHandler
	 */
	monitorDeployment(): express.RequestHandler {
		return async function(req: express.Request, res: express.Response) {
			try {
				// TODO: SSE reply with the deployment status
			}
			catch (e) {
				console.log('[@lemmsoft/devops-tools/server]: error:', e)
				res.status(500).end()
			}
		}
	}
}
