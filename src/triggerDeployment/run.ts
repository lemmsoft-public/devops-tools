import {argv} from 'yargs'
import {triggerDeployment} from './triggerDeployment.script'

if (argv.h || argv.help) {
	console.log('TODO: help menu')
	process.exit(0)
}

triggerDeployment({
	configPath: (argv.c || argv.configFile) as string,
	profileConfigPath: (argv.cp || argv.configProfileFile) as string,
	coverageReportPath: (argv.crp || argv.coverageReportFile) as string
}).then(
	() => {
		console.log('[@lemmsoft/devops-tools/triggerDeployment]: Script completed.')
		process.exit(0)
	},
	(err) => {
		console.log(err)
		process.exit(1)
	}
)
