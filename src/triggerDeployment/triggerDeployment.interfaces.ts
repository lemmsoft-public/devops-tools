export interface ITriggerDeploymentConfig {
	autoDeployHost: string
	autoDeployToken: string
	minimumRequiredCoverageLevel?: number
}

export interface ITriggerDeploymentOptions {
	configPath?: string
	profileConfigPath?: string
	coverageReportPath?: string
}
