import * as fetch from 'isomorphic-fetch'
import * as fs from 'fs-extra'
import {ITriggerDeploymentConfig, ITriggerDeploymentOptions} from './triggerDeployment.interfaces'
import * as merge from 'deepmerge'
import * as path from 'path'

/**
 * Processes the coverage report (if provided) and triggers a deployment sequence by calling the provided devops API address.
 * @param config (optional) - An object, containing the paths to the various configuration files.
 */
export async function triggerDeployment(options?: ITriggerDeploymentOptions): Promise<void> {
	const opt = options || ({} as ITriggerDeploymentOptions)
	let config: ITriggerDeploymentConfig | null = null
	if (opt.configPath) {
		config = (
			await import(
				opt.configPath.match(/^\//) ?
					opt.configPath :
					path.join(process.cwd(), opt.configPath)
			) || {}
		) as ITriggerDeploymentConfig
	}
	if (opt.profileConfigPath) {
		config = merge(
			config || {},
			await import(
				opt.profileConfigPath.match(/^\//) ?
					opt.profileConfigPath :
					path.join(process.cwd(), opt.profileConfigPath)
			) || {}
		) as ITriggerDeploymentConfig
	}
	if (!config || !config.autoDeployHost) {
		throw new Error(`[@lemmsoft/devops-tools/triggerDeployment]: Invalid config file(s) provided.`)
	}
	if (config.minimumRequiredCoverageLevel && opt.coverageReportPath) {
		console.log('[@lemmsoft/devops-tools/triggerDeployment]: Extracting coverage data...')
		const coverageData = (
			await fs.readFile(
				opt.coverageReportPath.match(/^\//) ?
					opt.coverageReportPath :
					path.join(process.cwd(), opt.coverageReportPath)
			)
		).toString()
		// let coverageLevel = parseFloat(coverageData.match(/strong">([\d\.]+)%/g)[0].match(/[\d\.]+/)[0])
		let coverageLevel: string[] | number | null = coverageData.match(/strong">([\d\.]+)%/g)
		if (!(coverageLevel instanceof Array) || !coverageLevel[0]) {
			throw new Error(`[@lemmsoft/devops-tools/triggerDeployment]: No valid coverage data found, expected an lcov coverage-report html file.`)
		}
		coverageLevel = coverageLevel[0].match(/[\d\.]+/)
		if (!coverageLevel || !coverageLevel[0]) {
			throw new Error(`[@lemmsoft/devops-tools/triggerDeployment]: No valid coverage data found, expected an lcov coverage-report html file.`)
		}
		coverageLevel = parseFloat(coverageLevel[0])
		if (isNaN(coverageLevel) || (coverageLevel < config.minimumRequiredCoverageLevel)) {
			throw new Error(`[@lemmsoft/devops-tools/triggerDeployment]: Insufficient coverage level at ${coverageLevel}%.`)
		}
		console.log(`[@lemmsoft/devops-tools/triggerDeployment]: Found a coverage level at ${coverageLevel}%, continuing.`)
	}
	console.log(`[@lemmsoft/devops-tools/triggerDeployment]: Calling the deployment API at ${config.autoDeployHost}/deploy...`)
	await fetch(
		`${config.autoDeployHost}/deploy`, {
			method: 'get',
			headers: {
				Authorization: `Bearer ${config.autoDeployToken}`
			}
		}
	)
}
