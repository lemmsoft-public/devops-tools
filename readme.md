![npm](https://img.shields.io/npm/v/@lemmsoft/devops-tools.svg)
![node](https://img.shields.io/node/v/@lemmsoft/devops-tools.svg)
![pipeline](https://gitlab.com/lemmsoft-public/devops-tools/badges/master/pipeline.svg)
[![Coverage Status](https://coveralls.io/repos/gitlab/lemmsoft-public/devops-tools/badge.svg?branch=master)](https://coveralls.io/gitlab/lemmsoft-public/devops-tools?branch=master)
![npm](https://img.shields.io/npm/dt/@lemmsoft/devops-tools.svg)
![npm](https://img.shields.io/npm/dm/@lemmsoft/devops-tools.svg)
<br/>

# Lemmsoft Devops Tools
A devops toolkit for automatic post-pipeline deployments on Linux VMs using Docker.<br/>
How it works:<br/>

## Table Of Contents
- [Getting Started](#getting-started)
- [Config File Reference](#config-file-reference)
- [Full CLI Reference](#full-cli-reference)

# Getting Started