let thereWereErrors = false
afterEach(function() {
	if (!thereWereErrors && (this.currentTest.state !== 'passed')) {
		thereWereErrors = true
	}
})
after(function() {
	setTimeout(
		() => {
			if (thereWereErrors) {
				process.exit(1)
			}
			process.exit(0)
		},
		1000
	)
})
ramster.runTests({
	exitProcessOnModuleTestFail: (argv.exitProcessOnModuleTestFail === 'true') || (argv.exitProcessOnModuleTestFail === true),
	testConfig: (argv.testConfig === 'true') || (argv.testConfig === true),
	testDB: (argv.testDB === 'true') || (argv.testDB === true),
	testDBInjectedModules: (argv.testDBInjectedModules === 'true') || (argv.testDBInjectedModules === true),
	testEmails: (argv.testEmails === 'true') || (argv.testEmails === true),
	testClients: (argv.testClients === 'true') || (argv.testClients === true) ? true : argv.testClients,
	testCronJobs: (argv.testCronJobs === 'true') || (argv.testCronJobs === true) ? true : argv.testCronJobs,
	testAPIs: (argv.testAPIs === 'true') || (argv.testAPIs === true) ? true : argv.testAPIs,
	testWebpackBuildTools: (argv.testWebpackBuildTools === 'true') || (argv.testWebpackBuildTools === true),
	staticDataFileNames: []
})
if (argv.testLint === 'true') {
	ramster.runLintTests(__dirname, `{,!(node_modules)/**/}*.js`, 'public')
}
